import React, { Component } from 'react';
import './App.css';
import BtnBackground from './components/BtnBackground'
import BtnColor from './components/BtnColor'
import BtnName from './components/BtnName'
import Box from './components/Box'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Box/>
        <BtnBackground/>
        <BtnColor/>
        <BtnName/>
      </div>
    );
  }
}