const TOGGLE_BACKGROUND = 'TOGGLE_BACKGROUND'
const TOGGLE_COLOR = 'TOGGLE_COLOR'
const TOGGLE_NAME = 'TOGGLE_NAME'

export const toggleName = () => {
  return {
    type: TOGGLE_NAME
  }
}

export const toggleBackground = () => {
  return {
    type: TOGGLE_BACKGROUND
  }
}

export const toggleColor = () => {
  return {
    type: TOGGLE_COLOR
  }
}