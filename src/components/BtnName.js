import React from 'react';
import { connect } from 'react-redux';
import { toggleName } from '../actions/actions';
import { bindActionCreators } from 'redux';


const BtnName = ({ onClick}) => (
  <div className='btn btn_name' onClick={onClick}>name</div>
)

function mapDispatchToProps(dispatch) {
  return {
    onClick: bindActionCreators(toggleName, dispatch)
  }
}

export default connect(
  store => ({
    store: store.name.value
  }),
  mapDispatchToProps)(BtnName)