import React from 'react';
import { connect } from 'react-redux';
import { toggleBackground } from '../actions/actions';
import { bindActionCreators } from 'redux';


const BtnBackground = ({onClick}) => (
  <div className='btn btn_background' onClick={onClick}>
    background
  </div>
)

function mapDispatchToProps(dispatch) {
  return {
    onClick: bindActionCreators(toggleBackground, dispatch)
  }
}

export default connect(
  store => ({
    store: store.background.value
  }),
  mapDispatchToProps)(BtnBackground)