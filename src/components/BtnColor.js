import React from 'react';
import { connect } from 'react-redux';
import { toggleColor } from '../actions/actions';
import { bindActionCreators } from 'redux';


const BtnColor = ({ onClick}) => (
  <div className='btn btn_color' onClick={onClick}>color</div>
)

function mapDispatchToProps(dispatch) {
  return {
    onClick: bindActionCreators(toggleColor, dispatch)
  }
}

export default connect(
  store => ({
    store: store.color.value
  }),
  mapDispatchToProps)(BtnColor)