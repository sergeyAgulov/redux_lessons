import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames'

function Box({store}) {
  const color = store.color.value
  const background = store.background.value
  const boxClass = classNames(
    'box',
    {
      'box_colored': color,
      'box_backgrounded': background,
    })
  return <div className={boxClass}>{store.name.value}</div>
}


export default connect(
  store => ({
    store: store
  })
)(Box);