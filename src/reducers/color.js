const TOGGLE_COLOR = 'TOGGLE_COLOR'
const initState = {
  value: false
}

export default function color (state = initState, action) {
  if (action.type === TOGGLE_COLOR) {
    return {
      value: !state.value
    }
  }
  return state
}

export function toggleColor() {
  return { type: TOGGLE_COLOR }
}