import { combineReducers } from 'redux';

import background from './background';
import color from './color';
import name from './name';

export default combineReducers({
  background,
  color,
  name
})