const TOGGLE_BACKGROUND = 'TOGGLE_BACKGROUND'
const initState = {
  value: false
}

export default function background (state = initState, action) {
  if (action.type === TOGGLE_BACKGROUND) {
    return {
      value: !state.value
    }
  }
  return state
}

export function toggleBack() {
  return { type: TOGGLE_BACKGROUND }
}