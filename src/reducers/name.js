const TOGGLE_NAME = 'TOGGLE_NAME'
const initState = {
  value: ''
}

const str = 'Серёжка'

export default function background(state = initState, action) {
  if (action.type === TOGGLE_NAME) {
    if (state.value === '') {
      return {
        value: str
      }
    } else {
      return {
        value: ''
      }
    }
  }
  return state
}

export function toggleBack() {
  return { type: TOGGLE_NAME }
}